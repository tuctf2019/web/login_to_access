# Login to Access

First a little SQLi, then a page backup -> a flag

* Desc: If at first you don't login, maybe back up.
* Flag: `TUCTF{b4ckup5_0f_php?_1t5_m0r3_c0mm0n_th4n_y0u_th1nk}`
* Hint: What happens when someone edits dynamic content?

## How To:
1. Congrats it's basic SQLi "`' or 1=1;--  `" works on `index.html`. The two spaces after the comment are important b/c this is PHP andthat's how PHP is.
2. Now you're on `login.html`. You try the same thing, nothing happens. It's because this is the `maybe back up` part of the chal. Both `login.html` and `index.html` send `POST` requests to `login.php`. This page is dynamically rendered...
3. But by throwing `.bak` at the end (`login.php.bak`) you see the same page with the flag
